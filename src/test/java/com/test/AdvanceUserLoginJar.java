package com.test;

import org.testng.TestNG;

import com.test.advanceusertest.AdvanceUserLogin;

public class AdvanceUserLoginJar {
	static TestNG testNg;

	public static void main(String[] args) {
		testNg = new TestNG();
		testNg.setTestClasses(new Class[] {AdvanceUserLogin.class});
		testNg.run();

	}

}
