package com.test.advanceusertest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;

public class CompleteAccountMgmtTest {
	WebDriver driver;
	Properties prop;
	File file;

	@Test(priority = 1)
	public void succesfullyLoginAdvanceUser() throws InterruptedException {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000);
	}

	@Test(priority = 2, dependsOnMethods = "succesfullyLoginAdvanceUser")
	public void clickOnAccountMgmt() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);
		WebElement Account = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[11]/span"));
		executor.executeScript("arguments[0].click()", Account);
		System.out.println("-------------------------------");
		System.out.println("Successfully Click On Account Management");
	}

	@Test(priority = 3, dependsOnMethods = "clickOnAccountMgmt")
	public void successfullyCreateAccountForAdvanceUser() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement create = driver.findElement(By.xpath("//button[contains(text(),'Create account')]"));
		executor.executeScript("arguments[0].click()", create);
		System.out.println("--------------------------------------------");
		WebElement fname = driver.findElement(By.xpath("//input[@id='firstName']"));
		fname.sendKeys(prop.getProperty("FirstName"));
		System.out.println("Entered Details:-");
		System.out.println("First Name:-" + prop.getProperty("FirstName"));
		WebElement lname = driver.findElement(By.xpath("//input[@id='lastName']"));
		lname.sendKeys(prop.getProperty("LastName"));
		System.out.println("Last Name:-" + prop.getProperty("LastName"));
		WebElement advemail = driver.findElement(By.xpath("//input[@id='email']"));
		advemail.sendKeys(prop.getProperty("EmailId"));
		WebElement confirmEmail = driver.findElement(By.xpath("//input[@id='confirmemail']"));
		confirmEmail.sendKeys(prop.getProperty("EmailId"));
		System.out.println("EmailId:-" + prop.getProperty("EmailId"));

		WebElement pcode1 = driver.findElement(By.xpath("//input[@id='password1']"));
		pcode1.sendKeys("1");
		WebElement pcode2 = driver.findElement(By.xpath("//input[@id='password2']"));
		pcode2.sendKeys("2");
		WebElement pcode3 = driver.findElement(By.xpath("//input[@id='password3']"));
		pcode3.sendKeys("3");
		WebElement pcode4 = driver.findElement(By.xpath("//input[@id='password4']"));
		pcode4.sendKeys("4");

		WebElement confpasscode1 = driver.findElement(By.xpath("//input[@id='confirmPassword1']"));
		confpasscode1.sendKeys("1");
		WebElement confpasscode2 = driver.findElement(By.xpath("//input[@id='confirmPassword2']"));
		confpasscode2.sendKeys("2");
		WebElement confpasscode3 = driver.findElement(By.xpath("//input[@id='confirmPassword3']"));
		confpasscode3.sendKeys("3");
		WebElement confpasscode4 = driver.findElement(By.xpath("//input[@id='confirmPassword4']"));
		confpasscode4.sendKeys("4");

		Select UserType = new Select(driver.findElement(By.xpath("//SELECT[@id='role']")));
		UserType.selectByValue("ADVANCE");
		System.out.println("Select User Type:-" + "ADVANCE");

		WebElement Smit = driver.findElement(By.xpath("//input[@value='DONE']"));
		Smit.submit();
		System.out.println("Successfully Create Account For Advance User");
	}

	@Test(priority = 4, dependsOnMethods = "clickOnAccountMgmt")
	public void successfullyEditAccountInAccountMgmt() throws Exception {
		// JavascriptExecutor executor = (JavascriptExecutor) driver;
		Thread.sleep(10000);
		System.out.println("--------------------------------------------");
		// driver.findElement(By.xpath(prop.getProperty("CheckBoxId"))).click();
		WebElement dr = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-accountmanagement-page/div/div/div/div/div/div[2]/div[2]/table/tbody/tr[11]/td[1]/mat-checkbox"));
		dr.click();
		System.out.println(dr.getText());
		System.out.println(dr.getClass());
//		WebElement selectaccount = driver.findElement(By.xpath("Selectedaccount"));
//		System.out.println("Selected Account:-" + selectaccount.getText());
//
//		WebElement edit = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-accountmanagement-page/div/div/div/div/div/div[2]/div[1]/div[2]/div/div[1]/button"));
//		executor.executeScript("arguments[0].click()", edit);
//
//		WebElement ename = driver.findElement(By.xpath("//input[@id='firstName']"));
//		ename.clear();
//		ename.sendKeys(prop.getProperty("EditName"));
//		System.out.println("Edit Name:-" + prop.getProperty("EditName"));
//		WebElement done = driver.findElement(By.xpath("//input[@value='SAVE']"));
//		done.submit();
		System.out.println("---------------------------------------------------");
		// driver.close();
	}

	@Test(priority=5)
	public void checkDataFromDB() throws Exception
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection
	    ("jdbc:mysql://104.198.155.142:3306/hexwave_device_setting?createDatabaseIfNotExist=true", "hexwave", "h3xW@veDevAugust13");

		Statement stmt = con.createStatement();
		String mail=prop.getProperty("EmailId");
		String gmail = mail;
		mail.split(".");
		System.out.println(mail);
		System.out.println(gmail);
		ResultSet rs = stmt.executeQuery("SELECT * FROM  keycloak.user_entity where EMAIL='"+gmail+"'");
		while(rs.next())	
		{
			System.out.print(rs.getString("ID")+"   ");
			System.out.print(rs.getDate("creation_date")+"   ");
			System.out.print(rs.getDate("update_date")+"   ");
			System.out.print(rs.getString("tablet_mac_address")+"   ");
			System.out.print(rs.getString("tablet_name"));
			System.out.print(rs.getBoolean("tablet_status"));

			System.out.println();
		}
	}
//	@Test(priority = 5, dependsOnMethods = "clickOnAccountMgmt")
//	public void successfullyDeleteAccountInAccountMgmt() throws InterruptedException {
//		JavascriptExecutor executor = (JavascriptExecutor) driver;
//		System.out.println("--------------------------------------------------------");
//		driver.findElement(By.xpath(prop.getProperty("deletecheckbox"))).click();
//		WebElement deletename=driver.findElement(By.xpath("deleteid"));
//		System.out.println("Delete Account Id:-"+deletename.getText());
//
//		WebElement delete = driver.findElement(By.xpath("//button[contains(text(),'Delete account')]"));
//		executor.executeScript("arguments[0].click()", delete);
//
//		WebElement cnfrm = driver.findElement(By.xpath("//button[contains(text(),'CONFIRM')]"));
//		executor.executeScript("arguments[0].click()", cnfrm);
//		Thread.sleep(1000);
//		WebElement cross = driver.findElement(By.xpath(
//				"/html/body/div[2]/div[2]/div/mat-dialog-container/app-alert-component/div/div[1]/div[3]/button"));
//		cross.click();
//		System.out.println("Successfully Delete Account In Account Management");
//		System.out.println("---------------------------------------------------");
//
//	}

	@BeforeTest
	public void beforeTest() throws IOException {

		WebDriverManager.chromedriver().setup();
//			ChromeOptions options = new ChromeOptions();
//			options.addArguments("headless");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);

		driver.get(prop.getProperty("URL"));
		System.out.println("Url :-" + prop.getProperty("URL"));
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		Thread.sleep(5000);
		// driver.close();
		System.out.println("All Test Cases Pass");

	}

}
