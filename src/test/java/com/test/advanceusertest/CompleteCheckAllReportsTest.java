package com.test.advanceusertest;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class CompleteCheckAllReportsTest {
	WebDriver driver;
	Properties prop;

	@Test(priority = 1)
	public void succesfullyLoginAdvanceUser() throws InterruptedException {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000);
	}

	@Test(priority = 2, dependsOnMethods = "succesfullyLoginAdvanceUser")
	public void clickOnReports() throws InterruptedException {
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Click On Reports");
		Thread.sleep(2000);
	}

	@Test(priority = 3, dependsOnMethods = "clickOnReports")
	public void successfullyShowReportDashBoard() throws InterruptedException {
		Thread.sleep(2000);
		WebElement RDBoard = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[1]"));
		RDBoard.click();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Succcessfully Show Report DashBaoard");
		Thread.sleep(2000);

	}

	@Test(priority = 4, dependsOnMethods = "clickOnReports")
	public void successfullyShowThroughputReport() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);

		WebElement Thput = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[2]/span[2]"));
		executor.executeScript("arguments[0].click()", Thput);

		WebElement show = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='Show report']"));
		executor.executeScript("arguments[0].click()", show);

		Thread.sleep(7000);

		WebElement Export = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='EXPORT TO PDF']"));
		executor.executeScript("arguments[0].click()", Export);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Show Throrughput Report and Export Data Into Excel");
	}

	@Test(priority = 5, dependsOnMethods = "clickOnReports")
	public void successfullyShowThreatActivityReport() throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);
		WebElement Thactivity = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[3]/span[2]"));
		executor.executeScript("arguments[0].click()", Thactivity);

		WebElement month = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='Show report ']"));
		month.click();

		WebElement show = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='Show report ']"));
		executor.executeScript("arguments[0].click()", show);

		Thread.sleep(10000);

		WebElement Export = driver.findElement(By.xpath("//BUTTON[@id='exportbtn']"));
		executor.executeScript("arguments[0].click()", Export);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Show Threat Activity Report And Export Data Into Excel");
		Thread.sleep(2000);

	}

	@Test(priority = 6, dependsOnMethods = "clickOnReports")
	public void successfullyShowThreatsReport() throws InterruptedException {
		Thread.sleep(2000);

		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);
		WebElement Threport = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[4]/span[2]"));
		executor.executeScript("arguments[0].click()", Threport);

		WebElement show = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='Show report ']"));
		executor.executeScript("arguments[0].click()", show);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Show Threats Report And Export Data Into Excel");
		Thread.sleep(2000);

	}

	@Test(priority = 7, dependsOnMethods = "clickOnReports")
	public void successfullyShowAnalyticReport() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);
		WebElement analytic = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[5]/span[2]"));
		executor.executeScript("arguments[0].click()", analytic);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Show Analytic Report");
		Thread.sleep(2000);

	}

	@Test(priority = 8, dependsOnMethods = "clickOnReports")
	public void successfullyShowBetaTestModeReport() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);

		WebElement Rport = driver.findElement(By.id("mat-expansion-panel-header-0"));
		executor.executeScript("arguments[0].click()", Rport);
		WebElement beta = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/mat-expansion-panel/div/div/a[6]/span[2]"));

		executor.executeScript("arguments[0].click()", beta);

		WebElement CheckReport = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-threatlogsreport/div/div/div/div/div/div[1]/div/div/form/div[4]/div[1]/button"));
		executor.executeScript("arguments[0].click()", CheckReport);

		Thread.sleep(5000);

		WebElement Export = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-threatlogsreport/div/div/div/div/div/div[1]/div/div/form/div[4]/div[3]/button"));
		executor.executeScript("arguments[0].click()", Export);
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Show Beta Test Mode Report And Export Data Into Excel");
		System.out.println("-------------------------------------------------------------------");
		Thread.sleep(2000);
		driver.close(); 
	}

	@BeforeTest
	public void beforeTest() throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		prop = new Properties();
		FileInputStream ip = new FileInputStream("./config2.properties");
		prop.load(ip);
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("All Test Cases Pass");

	}

}
