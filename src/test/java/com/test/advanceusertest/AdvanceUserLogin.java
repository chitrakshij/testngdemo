package com.test.advanceusertest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AdvanceUserLogin {
	WebDriver driver;
	Properties prop;
	File file;
	Connection con;

	@Test(priority = 1)
	public void successfullyLoginAdvanceUser() throws InterruptedException, IOException {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");

		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading..........");
		driver.get(prop.getProperty("URL"));
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));

		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		System.out.println("Passcode:-" + "****");
		System.out.println("Successfull Login Advance User");
		Thread.sleep(5000);
	}
	@Test(priority = 2, dependsOnMethods = "successfullyLoginAdvanceUser")
	public void successfullyLogoutFromAdvanceUser() 
	{

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		WebElement logout = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[14]/span"));
		executor.executeScript("arguments[0].click()", logout);
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("Successfully Logout From Advance User");
		System.out.println("----------------------------------------------------------------------------");
		driver.close();
	}


	@Test(priority = 3)
	public void successfullyCheckForWrongPassword() throws IOException, InterruptedException {
		WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");

		driver = new ChromeDriver(options);	
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading..........");
		driver.get(prop.getProperty("URL"));
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));

		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode5"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		System.out.println("Password Is Wrong:-" + "Unsuccessfull Login");
		System.out.println("----------------------------------------------------------------------------");

		Thread.sleep(5000);
		driver.close(); 

	}
	@Test(priority=4)
	public void checkDataFromDB() throws Exception
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection
	    ("jdbc:mysql://104.198.155.142:3306/hexwave_device_setting?createDatabaseIfNotExist=true", "hexwave", "h3xW@veDevAugust13");

		Statement stmt = con.createStatement();
		String mail=prop.getProperty("MailId");
		//String gmail = mail;
//		mail.split(".");
//		System.out.println(mail);
//		System.out.println(gmail);
		ResultSet rs = stmt.executeQuery("SELECT * FROM  keycloak.user_entity where EMAIL='"+mail+"'");
		while(rs.next())	
		{
			System.out.println("Details of user from the Database that we login:-");
			System.out.println("Id of user:-"+rs.getString("ID")+"   ");
			System.out.println("Email Id of user:-"+rs.getString("EMAIL"));
			System.out.println("First name of user:-"+rs.getString("FIRST_NAME"));
			System.out.println("Last Name of user:-"+rs.getString("LAST_NAME"));
			System.out.println("UserName for login:-"+rs.getString("USERNAME"));
			
			System.out.println();
			System.out.println("----------------------------------------------------------------------------");

		}
	}
	
	@BeforeTest
	public void afterTest() throws IOException, Exception {
		WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);

		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://104.198.155.142:3306/hexwave?createDatabaseIfNotExist=true",
				"hexwave", "h3xW@veDevAugust13");
	}

}
