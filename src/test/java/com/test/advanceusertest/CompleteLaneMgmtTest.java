package com.test.advanceusertest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class CompleteLaneMgmtTest
{
	WebDriver driver;
	Properties prop;
	File file;
	@Test(priority=1)
	public void succesfullyLoginAdvanceUser() throws InterruptedException
	{
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000); 
	}
  @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
  public void clickOnLaneMgmt() 
  {
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
	executor.executeScript("arguments[0].click()", nav_btn);
	WebElement lane = driver.findElement(By.xpath(
					"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[6]"));
	lane.click();
	System.out.println("-------------------------------------------------------------------");
	System.out.println("Successfully Click On Lane Management");
  }
  @Test(priority=3,dependsOnMethods = "clickOnLaneMgmt")
  public void successfullySwitchToGates() throws InterruptedException
  {
	  	Thread.sleep(2000);
	  	JavascriptExecutor executor = (JavascriptExecutor)driver;

	  	WebElement gateselect = driver.findElement(By.xpath(prop.getProperty("GateSelect")));
		executor.executeScript("arguments[0].click()", gateselect);
		WebElement SGate=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-hexwavepairdetails/div/div/div/div/div[1]/div[2]/div[1]/div[2]/div/div[2]/button"));
		SGate.click();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Swtich To Gates");
		
  }
  @Test(priority=4,dependsOnMethods = "clickOnLaneMgmt")
  public void successfullyEditLaneInLaneMgmt() throws InterruptedException
  {
	  Thread.sleep(2000);
	  	JavascriptExecutor executor = (JavascriptExecutor)driver;

	  	WebElement gateselect = driver.findElement(By.xpath(prop.getProperty("GateSelect")));
		executor.executeScript("arguments[0].click()", gateselect);
		Thread.sleep(2000);
		WebElement chckbox = driver.findElement(By.xpath(prop.getProperty("LaneSelect")));
		executor.executeScript("arguments[0].click()", chckbox);
		Thread.sleep(2000);
		WebElement editlane=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-hexwavepairdetails/div/div/div/div/div[1]/div[2]/div[1]/div[2]/div/div[4]/button"));
		editlane.click();
		WebElement editName=driver.findElement(By.id("Name"));
		editName.clear();
		editName.sendKeys(prop.getProperty("EditLaneName"));
		WebElement done=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-editlane/div/div/div/div/div[2]/div/form/div[4]/div[9]/div/div/input"));
		executor.executeScript("arguments[0].click()", done);
		System.out.println("-------------------------------------------------------------------");
		WebElement gatename=driver.findElement(By.xpath(prop.getProperty("selectgatename")));
		System.out.println("Selecting Gate :-" +"  " +  gatename.getText());
		WebElement lanename=driver.findElement(By.xpath(prop.getProperty("selectLaneName")));
		System.out.println("Selecting Lane:-" + lanename.getText());
		System.out.println("Successfully Edit Lane In Lane Management");
		System.out.println("-------------------------------------------------------------------");
		driver.close();
  }
  
  @BeforeTest
  public void beforeTest() throws IOException 
  {
	  	WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
  }

  @AfterSuite
  public void afterSuite() 
  {
	  System.out.println("All Test Cases Pass");

  }

}
